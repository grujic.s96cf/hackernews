import React from "react";
import "./App.scss";
import MainPageList from "./components/MainPageList";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import StoryComments from "./components/StoryComments";

function App() {
  return (
    <Router>
      <div className="App">
        <Link to="/">
          <h1>Hacker News</h1>
        </Link>
        <Switch>
          <Route exact path="/" component={MainPageList} />
          <Route exact path="/item/:storyId" component={StoryComments}></Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
