export const props = ({
  id,
  by,
  url,
  descendants,
  title,
  score,
  kids,
  text
} = {}) => ({
  id,
  by,
  url,
  descendants,
  title,
  score,
  kids,
  text
});
