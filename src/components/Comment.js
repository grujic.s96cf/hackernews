import React, { useState, useEffect } from "react";
import { getStory } from "../APIs/api";

function Comment({ commentId }) {
  const [story, setStory] = useState({});
  const [expand, setExpand] = useState(false);

  useEffect(() => {
    let _isMounted = true;
    getStory(commentId).then(data => _isMounted && setStory(data));
    return () => (_isMounted = false);
  }, []);

  const handleExpand = () => {
    setExpand(!expand);
  };

  return (
    <div className="comment">
      <span> by {story.by}</span>
      <div dangerouslySetInnerHTML={{ __html: story.text }}></div>
      {story.kids && story.kids.length > 0 && (
        <div>
          <span onClick={handleExpand}>
            [{expand ? "hide" : "see"}] {story.kids.length} replies to{" "}
            {story.by}'s comment
          </span>
          {expand &&
            story.kids.map(commentId => (
              <Comment key={commentId} commentId={commentId} />
            ))}
        </div>
      )}
    </div>
  );
}

export default Comment;
