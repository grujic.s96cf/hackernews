import React, { useState, useEffect } from "react";
import { getStoryIds } from "../APIs/api";
import Story from "./Story";
import { useInfiniteScroll } from "../scrollHook/infiniteScroll";

function MainPageList() {
  const [storyIds, setStoryIds] = useState([]);
  const { count } = useInfiniteScroll();

  useEffect(() => {
    getStoryIds().then(data => setStoryIds(data));
  }, []);

  return (
    <div>
      {storyIds.slice(0, count).map(storyId => (
        <Story key={storyId} storyId={storyId} />
      ))}
    </div>
  );
}

export default MainPageList;
