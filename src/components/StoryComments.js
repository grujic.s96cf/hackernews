import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { getStory } from "../APIs/api";
import Comment from "./Comment";

function StoryComments() {
  const [story, setStory] = useState({});
  const { storyId } = useParams();

  useEffect(() => {
    getStory(storyId).then(data => setStory(data));
  }, []);

  return (
    <div className="comment-page">
      <div className="story-info">
        <a href={story.url} target="_blank">
          {story.title}
        </a>
        <div>
          {story.score} points by {story.by}
        </div>
      </div>
      <div>
        <div className="comments-count">
          {story.kids && story.kids.length} comments on this story
        </div>
        {story.kids
          ? story.kids.map(commentId => (
              <Comment commentId={commentId} key={commentId} />
            ))
          : null}
      </div>
    </div>
  );
}

export default StoryComments;
