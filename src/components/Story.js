import React, { useState, useEffect } from "react";
import { getStory } from "../APIs/api";

function Story({ storyId }) {
  const [story, setStory] = useState({});

  useEffect(() => {
    getStory(storyId).then(data => setStory(data));
  }, []);

  return (
    <div className="story">
      <a href={story.url} target="_blank">
        {story.title}
      </a>
      <div>
        <span className="storyProp">{story.score} points </span>
        <span className="storyProp">by {story.by} </span>
        <a className="storyProp" href={`/item/${story.id}`}>
          {story.descendants} comments
        </a>
      </div>
    </div>
  );
}

export default Story;
