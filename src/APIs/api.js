import axios from "axios";
import { props } from "../properties/item";

export const baseUrl = "https://hacker-news.firebaseio.com/v0/";
export const topStoriesUrl = `${baseUrl}topstories.json`;
export const itemUrl = `${baseUrl}item/`;

//top stories
export const getStoryIds = async () => {
  const result = await axios.get(topStoriesUrl);

  return result.data;
};

//item id
export const getStory = async storyId => {
  const result = await axios.get(`${itemUrl + storyId}.json`);

  return props(result.data);
};
